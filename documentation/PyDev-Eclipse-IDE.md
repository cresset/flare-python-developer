# Using the PyDev Eclipse IDE for with Flare

[PyDev](http://www.pydev.org/index.html) is a Python IDE for [Eclipse](http://www.eclipse.org/) which can be use to run Flare Python scripts or remotely debug Flare extensions.

## Installation

1. Install Eclipse from http://www.eclipse.org/downloads/

2. Install PyDev for Eclipse using the instructions at http://www.pydev.org/manual_101_install.html.

3. Install PyDev for Flare by running:
> pyflare -m pip install --user pydevd


## Configuring the interpreter

1. In Eclipse select Window > Preferences > PyDev > Interpreters >Python Interpreter

2. Press "New...", set the name to pyflare and the executable to the "pyflare" executable located in the Flare installation directory. Press 'OK'.

3. Keep the defaults when asked to configure the 'pythonpath' and press 'OK'.

4. PyDev is now configured to use the pyflare Python Interpreter.

![](images/ConfigureEclipsePythonInterpreter.png)

## Running Scripts

1. In Eclipse select File > New > PyDev Project.

![](images/EclipseNewProjectWizard.png)

2. Enter a project name and set the interpreter to 'pyflare'. Press 'Finish' to create the project.

3. Select File > New > PyDev Module.

4. Give the module a name and press 'Finish'.

![](images/EclipseNewModule.png)

5. Select the 'Module: Main' template and press 'Ok'.

![](images/EclipseNewModuleSelectTemplate.png)

6. Now you have a Python module which you can start writing your script from.
Below is an example script which downloads a protein and prints its residues.
Copy the script into Eclipse and select File > Run to run it.

```python
import urllib.request

from cresset import flare

if __name__ == "__main__":
    project = flare.Project()

    url = "http://files.rcsb.org/view/1oit.pdb"
    response = urllib.request.urlopen(url)
    text = response.read().decode("utf-8")

    project.proteins.extend(flare.read_string(text, "pdb"))

    for residue in project.proteins[0].residues:
        print(residue)
```

![](images/EclipseRunningPyFlareScript.png)

## Remote Debugging Flare Extensions

1. Open the extension to debug in Eclipse by selection File > Open File. For example open the importexport.py extension.

2. Add a breaking point to a extension by inserting the line 'import pydevd; pydevd.settrace()'.
For example add the line to the start of the ImportExportExtension._export_ligand_table function.

![](images/EclipseSettrace.png)

3. In Eclipse switch to the Debug perspective via Window > Perspective > Open Perspective > Other > Debug.

4. Start the remote debugger server by selecting Pydev > Start Debug Server.

![](images/EclipseStartDebugServer.png)

5. Start Flare and and wait for it to run the 'import pydevd; pydevd.settrace()'. For this example the
'import pydevd; pydevd.settrace()' is only called when the 'Export Ligands Table' button on the 'Extensions' tab
is pressed.

6. Eclipse should now have hit the 'import pydevd; pydevd.settrace()' break point.

![](images/EclipseDebuggingExtension.png)
