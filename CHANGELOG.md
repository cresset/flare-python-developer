# Changelog

## 2025-01 for Flare 10.0
- No changes in this version

## 2024-06 for Flare 9.0
### Changed
- The REST example now contains a button to download data via REST (restexample.py)

## 2024-01 for Flare 8.0
- No changes in this version

## 2023-05-10 for Flare 7.0
- No changes in this version

## 2022-11-15 for Flare 6.1
- No changes in this version

## 2022-06-14 for Flare 6.0
- No changes in this version

## 2021-06-28 for Flare 5.0
- No changes in this version

## 2020-05-02 for Flare 4.0
- No changes in this version

## 2020-01-30 for Flare 3.0
### Changed
- Updates to support the Flare 3.0 Python API

### Removed
- The Python Notebook extension has been moved to the flare-python-extensions/featured repository

## 2018-11-28 for Flare 2.0
### Added
- Python Notebook extension (pythonnotebook)

## 2018-07-25 for Flare 2.0
### Added
- Example on how to create a GUI using QtDesigner (qtdesignerexample)
- Example on how to add controls to the ribbon (ribbon)
- Example on how to add context menu items (contextmenus.py)
- Example on how to download and add proteins (download1oit.py)
- Example on how to update ligand properties using a rest service (restexample.py, runrestservice.py)
