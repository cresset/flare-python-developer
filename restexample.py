# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Demonstrates how to use a rest server to add properties to Ligand table when ligands are added.

Callbacks:
    flare.callbacks.ligands_added
        When a ligand is added background threads are started which calculates a property.
        The background thread then ask the main thread to add a property to the ligand.
        This example by default generates a value on the background thread instead of using a REST
        server. To use a REST server modify the the _calculate_data() method.
"""
import threading
import time
import random
import requests
import traceback

from cresset import flare


@flare.extension
class RestExampleExtension:
    """Calculate properties on ligands via REST.

    This happens either when a ligand is added or when a ribbon button is pressed.
    """

    def load(self):
        """Start listening for molecules being added and adds a button to the ribbon."""
        flare.callbacks.ligands_added.add(self._on_ligands_added)

        tab = flare.main_window().ribbon["Developer"]
        tab.tip_key = "D"
        group = tab["REST"]
        control = group.add_button("Add column via REST", self._on_button_pressed)
        control.tooltip = "Make a request to a REST server and add the results as a column."
        control.tip_key = "R"

        print(f"Loaded {self.__class__.__module__}.{self.__class__.__name__}")

    def _on_button_pressed(self):
        """Called when the 'Add column via REST' button is pressed.

        Start a background thread which will calculate the property.
        """
        project = flare.main_window().project
        self._start_calculate_data_background_thread(project.ligands, "From Button Pressed")

    def _on_ligands_added(self, ligands):
        """Called when ligands are added to the ligand table.

        `ligands` contains the ligands that were added.

        Start a background thread which will calculate the property.
        """
        self._start_calculate_data_background_thread(ligands, "From Adding Ligands")

    def _start_calculate_data_background_thread(self, ligands, column_prefix):
        """Start a background thread which will calculate the property."""
        # Convert the ligand to SDF format and send it to the background thread
        for ligand in ligands:
            sdf = ligand.write_string("sdf")
            thread = threading.Thread(
                target=self._calculate_data, args=(ligand, sdf, column_prefix)
            )
            thread.start()

    def _calculate_data(self, ligand, sdf, column_prefix):
        """Calculate the property, then inform the main thread to update the ligand.

        This runs on the background thread.
        """
        # In this example we generate a random number instead of getting the data from a REST
        # service so that the unmodified version of this script does not send data to external
        # resources.
        #
        # Uncomment _data_for_ligand_rest and comment out _data_for_ligand_random and this
        # extension will instead send the SDF file to a rest server running on localhost.
        # See the script developer-examples-pyflare-scripts/runrestservice.py for an example
        # REST server which will work with _data_for_ligand_rest.

        data = self._data_for_ligand_random(sdf)
        # data = self._data_for_ligand_rest(sdf)

        # Send the calculated data back to the main thread so the ligand can be updated
        flare.invoke_later(target=self._update_ligands, args=(data, ligand, column_prefix))

    def _data_for_ligand_random(self, sdf):
        """Return a random number and wait for some time to simulate a request to a REST server.

        This runs on the background thread.
        """
        # In this example a random number is generated but for real applications we could
        # have sent the molecule off to a REST server and waited for a response. As this
        # function is being called on a background thread Flare can still be used
        # while waiting for the REST server.
        time.sleep(0.1)
        data = random.random()
        return round(data, 2)

    def _data_for_ligand_rest(self, sdf):
        """Send the `sdf` file to the server and return the response.

        See the developer-examples-pyflare-scripts/runrestservice.py script for
        an example of a REST service.

        This runs on the background thread.
        """
        data = None
        try:
            response = requests.get("http://localhost:8080/", data=sdf.encode("utf-8"))
            response.raise_for_status()
            data = response.text
        except requests.exceptions.RequestException as exc:
            data = str(exc)
            # Print the exception to the log
            traceback.print_exc()

        return data

    @staticmethod
    def _update_ligands(new_property, ligand, column_prefix):
        """On the main thread set the property on the ligand."""
        try:
            prop = ligand.properties[column_prefix + " Python Property"]
            prop.value = new_property
            prop.tooltip = "My Tooltip"

            # The color of the new property maty also be set
            color = (random.random(), random.random(), random.random())
            prop.foreground_color = color
            color = (random.random(), random.random(), random.random())
            prop.background_color = color
        except flare.ObjectDeletedError:
            pass  # Ignore ligands which have been deleted
